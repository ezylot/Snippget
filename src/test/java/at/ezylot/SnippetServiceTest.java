package at.ezylot;

import at.ezylot.entities.SnippetEntity;
import at.ezylot.repositories.SnippetRepository;
import at.ezylot.services.SnippetService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebIntegrationTest("server.port:0")
@ActiveProfiles("test")
public class SnippetServiceTest {

    @Autowired
    SnippetService snippetService;

    @Autowired
    SnippetRepository snippetRepository;

    Long savedSnippetId;

    @Before
    public void setUp() {
        SnippetEntity snippetEntity = new SnippetEntity();
        snippetEntity.setContent("testContent");
        snippetEntity.setName("testName");
        savedSnippetId = snippetService.saveOrUpdateSnippet(snippetEntity).getId();
    }

    @Test
    public void testGetById() {
        Assert.assertEquals("testName", snippetService.getSnippetById(savedSnippetId).getName());
    }

    @Test
    public void testSaveNewEntity() {
        SnippetEntity snippetEntity = new SnippetEntity();
        snippetEntity.setContent("newContent");
        snippetEntity.setName("newName");
        snippetEntity = snippetService.saveOrUpdateSnippet(snippetEntity);

        Assert.assertNotNull(snippetEntity);
        Assert.assertNotNull(snippetEntity.getId());
        Assert.assertNotEquals(new Long(0), snippetEntity.getId());

        SnippetEntity savedSnippet = snippetRepository.findByName("newName");

        Assert.assertEquals(snippetEntity.getContent(), savedSnippet.getContent());
        Assert.assertEquals(snippetEntity.getId(), savedSnippet.getId());
        Assert.assertEquals(snippetEntity.getName(), savedSnippet.getName());
    }

    @Test
    public void testUpdateEntity() {
        SnippetEntity savedSnippet = snippetRepository.findOne(savedSnippetId);
        Assert.assertEquals("testContent", savedSnippet.getContent());

        SnippetEntity newSnippet = new SnippetEntity();
        newSnippet.setContent("newContent");
        newSnippet.setName("testName");
        newSnippet.setId(savedSnippet.getId());
        snippetService.saveOrUpdateSnippet(newSnippet);

        savedSnippet = snippetRepository.findOne(savedSnippetId);
        Assert.assertEquals("newContent", savedSnippet.getContent());
    }



    @Test
    public void testDelete() {
        SnippetEntity snippetEntity = new SnippetEntity();
        snippetEntity.setContent("newContent");
        snippetEntity.setName("newName");
        snippetEntity = snippetService.saveOrUpdateSnippet(snippetEntity);

        Assert.assertNotNull(snippetEntity.getId());
        Assert.assertNotNull(snippetService.getSnippetById(snippetEntity.getId()));

        snippetService.deleteSnippetWithId(snippetEntity.getId());
        Assert.assertNull(snippetService.getSnippetById(snippetEntity.getId()));
    }

    @Test
    public void testGetSnippetsPaginated() {
        SnippetEntity snippetEntity = new SnippetEntity();
        snippetEntity.setContent("newContent");
        snippetEntity.setName("newName");
        snippetService.saveOrUpdateSnippet(snippetEntity);

        snippetEntity = new SnippetEntity();
        snippetEntity.setContent("newContent");
        snippetEntity.setName("newName");
        snippetService.saveOrUpdateSnippet(snippetEntity);

        snippetEntity = new SnippetEntity();
        snippetEntity.setContent("newContent");
        snippetEntity.setName("newName");
        snippetService.saveOrUpdateSnippet(snippetEntity);

        Page page = snippetService.getSnippetEntitiesPaginated(1, 1);
        Assert.assertEquals(1, page.getContent().size());
        Assert.assertTrue(page.getTotalElements() >= 3);
        Assert.assertTrue(page.getTotalPages() >= 2);

        page = snippetService.getSnippetEntitiesPaginated(1, 2);
        Assert.assertEquals(2, page.getContent().size());
        Assert.assertTrue(page.getTotalElements() >= 3);
        Assert.assertTrue(page.getTotalPages() >= 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSaveNull() {
        snippetService.saveOrUpdateSnippet(null);
    }
}
