package at.ezylot;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.EmbeddedWebApplicationContext;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebIntegrationTest("server.port:0")
@ActiveProfiles("test")
public class BasicFunctionalityTest {

    Log LOG = LogFactory.getLog(BasicFunctionalityTest.class);

    @Autowired
    EmbeddedWebApplicationContext server;

    @Value("${local.server.port}")
    int port = -1;

    @Before
    public void Startup() {
        LOG.debug("Server for testing was started on port: " + port);
    }

    @Test
    public void TestBasicAutowireFunction() {
        Assert.assertNotEquals(-1, port);
        Assert.assertNotNull(server);
    }
}
