package at.ezylot.services;


import at.ezylot.entities.SnippetEntity;
import at.ezylot.repositories.SnippetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

@Service
public class SnippetService {

    @Autowired
    SnippetRepository snippetRepository;

    @Autowired
    EntityManager em;

    /**
     * Saves or updates a snippet. It also replaces all \r\n with \n before saving
     *
     * @param snippet Snippet that should be stored to the database.
     *
     * @return The stored object (Attention: this object is attached to the persistence context and changes
     * will be automatically applied to stored object in the database.
     *
     * @throws IllegalArgumentException Thrown when the snippet is null, or the snippets name is null or empty.
     */
    public SnippetEntity saveOrUpdateSnippet(SnippetEntity snippet) {
        if(snippet == null) throw new IllegalArgumentException("Snippet must not be null");

        snippet.setName(snippet.getName().replace("\r\n", "\n"));
        snippet.setContent(snippet.getContent().replace("\r\n", "\n"));

        if(snippet.getId() != null) {
            SnippetEntity oldSnippet = snippetRepository.findOne(snippet.getId());
            if (oldSnippet != null) {
                oldSnippet.setName(snippet.getName());
                oldSnippet.setContent(snippet.getContent());
                snippetRepository.save(snippet);
                return snippet;
            }
        }
        return snippetRepository.save(snippet);
    }

    /**
     * Removes the snippet with the given ID
     *
     * @param id Id of the snippet that should be deleted
     */
    public void deleteSnippetWithId(Long id) {
        snippetRepository.delete(id);
    }

    /**
     * Returns a single Snippet that has the given ID
     * @param id ID of the snippet that should be looked up
     * @return Snippet with the given id
     */
    public SnippetEntity getSnippetById(Long id) {
        return snippetRepository.findOne(id);
    }

    /**
     * Returns a list of Snippets with the option to paginate the results
     * @param pageNumber 0-based index of the page that should be retrieved
     * @param numberOfSnippetsPerSite Amount of snippets that should be on one site
     * @return Paginated list of Snippets
     */
    public Page<SnippetEntity> getSnippetEntitiesPaginated(Integer pageNumber, Integer numberOfSnippetsPerSite) {
        Pageable page = new PageRequest(pageNumber-1, numberOfSnippetsPerSite);
        return snippetRepository.findAll(page);
    }
}
