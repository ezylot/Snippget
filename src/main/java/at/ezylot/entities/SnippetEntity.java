package at.ezylot.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity(name="snippet")
@Table(name="snippets")
public class SnippetEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    @NotNull(message = "{snippet.name.notNull}")
    @Size(min = 4, message = "{snippet.name.minSize}")
    private String name;

    @Size(max = 20000, message = "{snippet.content.maxSize}")
    private String content;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
