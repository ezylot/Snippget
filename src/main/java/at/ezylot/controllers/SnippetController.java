package at.ezylot.controllers;

import at.ezylot.entities.SnippetEntity;
import at.ezylot.services.SnippetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;


@Controller
public class SnippetController {

    @Autowired
    SnippetService snippetService;

    @RequestMapping(path = "snippet/new", method = RequestMethod.GET)
    public ModelAndView createSnippet() {
        ModelAndView mav = new ModelAndView();
        mav.addObject("snippet", new SnippetEntity());
        mav.setViewName("createSnippet");
        return mav;
    }

    @RequestMapping(path = "snippet/new", method = RequestMethod.POST)
    public ModelAndView createSnippet(@Valid @ModelAttribute("snippet") SnippetEntity snippet, BindingResult result, ModelAndView mav) {
        if(result.hasErrors()) {
            mav.setViewName("createSnippet");
            return mav;
        }

        mav.addObject("snippet", snippetService.saveOrUpdateSnippet(snippet));
        mav.setViewName("redirect:/snippet/list");
        return mav;
    }

    @RequestMapping(path = "/snippet/raw/{id}")
    @ResponseBody
    public String showSnippetRaw(@PathVariable Long id) {
        return "<pre>" + snippetService.getSnippetById(id).getContent() + "</pre>";
    }

    @RequestMapping(path = "/snippet/delete/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ModelAndView deleteSnippet(@PathVariable Long id, ModelAndView mav, HttpServletRequest httpServletRequest) {
        snippetService.deleteSnippetWithId(id);
        mav.setViewName("redirect:/snippet/list");
        return mav;
    }

    @RequestMapping(path = { "", "snippets", "snippet/list" }, method = RequestMethod.GET)
    public ModelAndView listSnippets(ModelAndView mav, HttpServletRequest httpServletRequest) {
        return listSnippets(mav, 1, httpServletRequest);
    }

    @RequestMapping(path = { "snippets/{pageNumber}", "snippet/list/{pageNumber}" } , method = RequestMethod.GET)
    public ModelAndView listSnippets(ModelAndView mav, @PathVariable Integer pageNumber, HttpServletRequest httpServletRequest) {
        Page page = snippetService.getSnippetEntitiesPaginated(pageNumber, 20);
        mav.addObject("pageNumber", pageNumber);
        mav.addObject("totalPages", page.getTotalPages());
        mav.addObject("snippets", page.getContent());
        mav.addObject("totalElements", page.getNumberOfElements());

        mav.addObject("username", httpServletRequest.getRemoteUser());
        mav.addObject("isAdmin", httpServletRequest.isUserInRole("ADMIN"));

        mav.setViewName("listSnippets");
        return mav;
    }
}
