package at.ezylot.repositories;

import at.ezylot.entities.SnippetEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SnippetRepository extends PagingAndSortingRepository<SnippetEntity, Long> {
    SnippetEntity findByName(String name);
}
